<?php

namespace App\Controller;

use App\Service\SiteUpdater;
use RouterOS;
use RouterOS\Client;
use RouterOS\Exceptions\ClientException;
use RouterOS\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;
use function App\Helpers\getClient;

class DefaultController extends AbstractController
{

    function test(): Response
    {
        return new JsonResponse(
            array(
                "successful" => true
            )
        );
    }


    function runQuery(Request $request): Response
    {
        try {


            $body = $request->request;

            $client = getClient($request);

            $query = new Query($body->get('query'));

            $result = $client->query($query)->read();

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function getState(Request $request): Response
    {
        try {

            $client = getClient($request);

            $query = new Query("/interface/print");

            $result = $client->query($query)->read();

            return new JsonResponse(array(
                "success" => true,
                "result" => count($result) > 0
            ));
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => $th->getMessage()
                ),
                500
            );
        }
    }
}
