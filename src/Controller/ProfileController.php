<?php

namespace App\Controller;

use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\QueryException;
use RouterOS\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use function App\Helpers\getClient;
use function App\Helpers\hasQueryFailure;
use function Symfony\Component\DependencyInjection\Loader\Configurator\ref;

class ProfileController
{
    function create(Request $request): Response
    {
        try {
            $body = $request->request;

            $client = getClient($request);

            $name = $body->get("name");
            $rateLimit = $body->get("rateLimit");

            $query = (new Query('/ppp/profile/add'))->setAttributes(array(
                "=name=$name",
                "=rate-limit=$rateLimit"
            ));

            $result = $client->query($query)->read();

            if (hasQueryFailure($result)) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "result" => $result
                    )
                );
            }

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function delete(Request $request): Response
    {
        try {
            $body = $request->request;

            $client = getClient($request);

            $name = $body->get('name');

            $reference = $client->query((new Query("/ppp/profile/print"))->where("name", $name))->read();

            if (count($reference) <= 0) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "result" => "This profile doesn't exist in our records"
                    )
                );
            }

            $profile = $reference[0];

            $query = (new Query("/ppp/profile/remove"))->equal(
                '.id', $profile['.id']
            );

            $result = $client->query($query)->read();

            return JsonResponse::create(
                array(
                    "success" => true,
                    "result" => $result
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function view(Request $request): Response
    {
        try {
            $name = $request->query->get('name');

            if ($name == null) {
                return $this->index($request);
            }

            $client = getClient($request);

            $query = new Query('/ppp/profile/print');

            $query->where('name', $name);

            $result = $client->query($query)->read();

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function index(Request $request): Response
    {

        try {

            $client = getClient($request);

            $query = new Query('/ppp/profile/print');

            $result = $client->query($query)->read();

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function update(Request $request): Response
    {
        try {
            $body = $request->request;

            $client = getClient($request);

            $name = $body->get('name');
            $rateLimit = $body->get('rate-limit');

            if ($name == null) {
                return new JsonResponse(
                    [
                        "success" => false,
                        "result" => "You must send name params"
                    ], 400
                );
            }

            $query = (new Query("/ppp/profile/print"))
                ->where("name", $name);

            $result = $client->query($query)->read();


            if (count($result) <= 0 || hasQueryFailure($result)) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "result" => $result ?? "This profile doesn't exist in our records"
                    )
                );
            }

            $profile = $result[0];

            $query = (new Query("/ppp/secret/set"))
                ->equal(".id", $profile['.id'])
                ->equal("name", $name)
                ->equal("rate-limit", $rateLimit ?? $profile['password']);

            $result = $client->query($query)->read();

            if (hasQueryFailure($result)) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "result" => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => true,
                    "result" => $result
                )
            );
        } catch (QueryException $query) {
            var_dump($query);
            return JsonResponse::create(
                array(
                    "success" => false,
                    "error" => $query
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }
}