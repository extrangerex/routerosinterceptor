<?php

namespace App\Controller;

use RouterOS\Exceptions\ClientException;
use RouterOS\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use function App\Helpers\getClient;

class ToolController
{
    function speedTest(Request $request): Response
    {

        try {
            $body = $request->request;

            $client = getClient($request);

            $address = $body->get("address");

            if ($address == null) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "result" => "You must specify an address"
                    )
                );
            }

            $query = (new Query('/tool/speed-test'))->setAttributes(array(
                "=address=$address"
            ));

            $result = $client->query($query)->read();

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function cpuUsage(Request $request): Response
    {

        try {
            $client = getClient($request);

            $query = (new Query('/tool/profile'))->equal("duration", "1");

            $result = $client->query($query)->read();

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }
}
