<?php

namespace App\Controller;

use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\QueryException;
use RouterOS\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use function App\Helpers\getClient;

class UserDatabaseController
{
    function update(Request $request): Response
    {
        try {
            $body = $request->request;

            $client = getClient($request);

            $disabled = $body->get('disabled');
            $name = $body->get('name');
            $password = $body->get('password');
            $callerId = $body->get('caller-id');
            $profile = $body->get('profile');
            $service = $body->get('service');

            if ($name == null) {
                return new JsonResponse(
                    [
                        "success" => false,
                        "result" => "You must send name params"
                    ], 400
                );
            }

            $query = (new Query("/ppp/secret/print"))
                ->where("name", $name);

            $result = $client->query($query)->read();

            if (count($result) <= 0) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "error" => "This user doesn't exist in our records"
                    )
                );
            }

            $user = $result[0];

            $query = (new Query("/ppp/secret/set"))
                ->equal(".id", $user['.id'])
                ->equal("name", $name ?? $user['name'])
                ->equal("password", $password ?? $user['password'])
                ->equal("service", $service ?? $user['service'])
                ->equal("profile", $profile ?? $user['profile'])
                ->equal("caller-id", $callerId ?? $user['caller-id'])
                ->equal("disabled", $disabled ?? $user['disabled']);

            $result = $client->query($query)->read();

            return new JsonResponse(
                array(
                    "success" => true,
                    "result" => $result
                )
            );
        } catch (QueryException $query) {
            var_dump($query);
            return JsonResponse::create(
                array(
                    "success" => false,
                    "error" => $query
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function disable(Request $request): Response
    {
        try {
            $body = $request->request;

            $client = getClient($request);

            $name = $body->get('name');

            $disabled = $body->get('disabled');

            if ($name == null && $disabled == null) {
                return new JsonResponse(
                    [
                        "success" => false,
                        "result" => "You must send name and disabled params"
                    ], 400
                );
            }

            $query = (new Query("/ppp/secret/print"))
                ->where("name", $name);

            $result = $client->query($query)->read();

            if (count($result) <= 0) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "error" => "This user doesn't exist in our records"
                    )
                );
            }

            $query = (new Query("/ppp/secret/set"))
                ->equal(".id", $result[0]['.id'])
                ->equal("disabled", $disabled);

            $result = $client->query($query)->read();

            return new JsonResponse(
                array(
                    "success" => true,
                    "result" => $result
                )
            );
        } catch (QueryException $query) {
            var_dump($query);
            return JsonResponse::create(
                array(
                    "success" => false,
                    "error" => $query
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function delete(Request $request): Response
    {

        try {
            $body = $request->request;

            $client = getClient($request);

            $name = $body->get('name');

            if ($name == null) {
                return new JsonResponse(
                    [
                        "success" => false,
                        "result" => "You must send name params"
                    ], 400
                );
            }

            $query = (new Query("/ppp/secret/print"))
                ->where("name", $name);

            $result = $client->query($query)->read();

            if (count($result) <= 0) {
                return JsonResponse::create(
                    array(
                        "success" => false,
                        "error" => "This user doesn't exist in our records"
                    )
                );
            }

            $query = (new Query("/ppp/secret/remove"))
                ->equal(".id", $result[0]['.id']);

            $result = $client->query($query)->read();

            return new JsonResponse(
                array(
                    "success" => true,
                    "result" => $result
                )
            );
        } catch (QueryException $query) {
            return JsonResponse::create(
                array(
                    "success" => false,
                    "error" => $query
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function index(Request $request): Response
    {

        try {

            $client = getClient($request);

            $query = new Query('/ppp/secret/print');

            $result = $client->query($query)->read();

            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => $result
                    )
                );
            }

            return new JsonResponse(
                array(
                    "success" => false
                )
            );
        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }

    function create(Request $request): Response
    {
        try {
            $body = $request->request;

            $client = getClient($request);

            $name = $body->get("name");

            $query = (new Query('/ppp/secret/print'))->where("name", $name);

            $result = $client->query($query)->read();



            if (count($result) > 0) {
                return new JsonResponse(
                    array(
                        'success' => true,
                        'result' => "This user is already in our records"
                    ), 409
                );
            }

            $password = $body->get("password");
            $mac = $body->get("mac");
            $service = $body->get("service") ?? "any";
            $profile = $body->get("profile");

            $query = (new Query("/ppp/profile/print"))->where("name", $profile);

            $result = $client->query($query)->read();

            if (count($result) <= 0) {
                return new JsonResponse([
                    "success" => false,
                    "result" => "This profile doesn't exist in our records"
                ], 400);
            }

            $remoteAddress = $body->get("remoteAddress");

            $query = (new Query('/ppp/secret/add', [
                "=name=$name",
                "=password=$password",
                "=caller-id=$mac",
                "=service=$service",
                "=profile=$profile",
                "=remote-address=$remoteAddress"
            ]));

            $result = $client->query($query)->read();

            if (count($result) > 0) {


                return new JsonResponse([
                    "success" => true,
                    'result' => $result
                ]);
            }

            return new JsonResponse(
                [
                    "success" => false
                ]
            );

        } catch (ClientException $exception) {
            return new JsonResponse(array(
                "success" => false,
                "error" => utf8_encode($exception->getMessage())
            ), 503);
        } catch (\Throwable $th) {
            return new JsonResponse(
                array(
                    "success" => false,
                    "error" => utf8_encode($th->getMessage())
                ),
                500
            );
        }
    }
}
