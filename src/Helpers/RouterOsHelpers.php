<?php

namespace App\Helpers;


use RouterOS\Client;
use Symfony\Component\HttpFoundation\Request;

/**
 * @throws \RouterOS\Exceptions\ClientException
 * @throws \RouterOS\Exceptions\ConnectException
 * @throws \RouterOS\Exceptions\QueryException
 * @throws \RouterOS\Exceptions\BadCredentialsException
 * @throws \RouterOS\Exceptions\ConfigException
 */
function getClient(Request $request): Client
{
    $body = $request->headers;

    return new Client([
        'host' => $body->get('host'),
        'user' => $body->get('user'),
        'pass' => $body->get('password'),
        'port' => 8728
    ]);
}

function hasQueryFailure($response): bool {
    return strpos(json_encode($response), "failure");
}